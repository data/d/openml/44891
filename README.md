# OpenML dataset: forest_fires

https://www.openml.org/d/44891

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The aim of this dataset is to predict the burned area of forest fires, in the northeast region of Portugal, by using meteorological and other data.

The output 'area' was first transformed with a $ln(x+1)$ function. Then, several Data Mining methods were applied. After fitting the models, the outputs were  post-processed with the inverse of the $ln(x+1)$ transform. Four different input setups were used.

**Attribute Description**

1. *X* - x-axis spatial coordinate within the Montesinho park map: 1 to 9
2. *Y* - y-axis spatial coordinate within the Montesinho park map: 2 to 9
3. *month* - month of the year: 'jan' to 'dec'
4. *day* - day of the week: 'mon' to 'sun'
5. *FFMC* - FFMC index from the FWI system: 18.7 to 96.20
6. *DMC* - DMC index from the FWI system: 1.1 to 291.3
7. *DC* - DC index from the FWI system: 7.9 to 860.6
8. *ISI* - ISI index from the FWI system: 0.0 to 56.10
9. *temp* - temperature in Celsius degrees: 2.2 to 33.30
10. *RH* - relative humidity in %: 15.0 to 100
11. *wind* - wind speed in km/h: 0.40 to 9.40
12. *rain* - outside rain in mm/m2 : 0.0 to 6.4
13. *area* - the burned area of the forest (in ha): 0.00 to 1090.84 (this target variable is very skewed towards 0.0, thus it may make sense to model with the logarithm transform).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44891) of an [OpenML dataset](https://www.openml.org/d/44891). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44891/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44891/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44891/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

